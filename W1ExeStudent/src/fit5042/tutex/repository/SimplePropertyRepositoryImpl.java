/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	ArrayList<Property> propertyList = new ArrayList<Property>();
	
    public SimplePropertyRepositoryImpl() {
        
    }

	@Override
	public void addProperty(Property property) {
		if(property != null) {
			propertyList.add(property);
		}
	}

	@Override
	public Property searchPropertyById(int id)  {
		Property matchedProperty = null;
		for (Property p : propertyList) {
			if (p.getId() == id) {
				matchedProperty = p;
			}				
		}
		return matchedProperty;
	}

	@Override
	public List<Property> getAllProperties()  {
		return propertyList;
	}
    
}
